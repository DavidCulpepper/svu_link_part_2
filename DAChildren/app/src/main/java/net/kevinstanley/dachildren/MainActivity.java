package net.kevinstanley.dachildren;

import android.content.DialogInterface;
import android.graphics.Color;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {
    private int count = 0;
    private Boolean isEdit = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        CheckBox check = (CheckBox) findViewById(R.id.chkChild);
        check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
                kidsChecked(isChecked);
            }
        }
        );


        findViewById(R.id.btnAddChild).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewChild();
            }
        });

        findViewById(R.id.btnEdit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                swapButton();
            }
        });

    }

    private void swapButton() {
        if(isEdit) {
            ((FloatingActionButton) findViewById(R.id.btnEdit)).setImageResource(R.drawable.save);
            isEdit = false;
        }
        else {
            ((FloatingActionButton) findViewById(R.id.btnEdit)).setImageResource(R.drawable.edit);
            isEdit = true;
        }
    }

    private void addNewChild()
    {
        LinearLayout grid = (LinearLayout)findViewById(R.id.gridChildList);
        final View v = getLayoutInflater().inflate(R.layout.childitem, grid, false);
        v.findViewById(R.id.btnDOB).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDOB(v);
            }
        });

        this.count++;
        if(count % 2 == 0)
            v.setBackgroundColor(Color.LTGRAY);
        else
            v.setBackgroundColor(Color.WHITE);

        grid.addView(v);


    }

    private void showDOB(final View view)
    {
        final DatePicker date = new DatePicker(this);

        new AlertDialog.Builder(this)
                .setTitle("Select Birthday")
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        setDOB(view, date.getYear(), date.getMonth(), date.getDayOfMonth());
                    }
                })
                .setNegativeButton(android.R.string.cancel, null)
                .setView(date)
                .show();

    }

    private void setDOB(View view, int year, int month, int dayOfMonth) {
        ((TextView) view.findViewById(R.id.txtDOB)).setText(month + "/" + dayOfMonth + "/" + year);
    }


    private void kidsChecked(Boolean isChecked)
    {
        if(isChecked)
        {
            findViewById(R.id.layoutAddChild).setVisibility(View.VISIBLE);
            findViewById(R.id.layoutChildList).setVisibility(View.VISIBLE);
        }
        else {
            findViewById(R.id.layoutAddChild).setVisibility(View.GONE);
            findViewById(R.id.layoutChildList).setVisibility(View.GONE);
        }
    }



}
